﻿using FlexCel.Core;
using FlexCel.XlsAdapter;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestCityInfo
{
    class ExportExcel
    {
        List<TestReport> companyReport;
        public ExportExcel(List<TestReport> companyReport)
        {
            this.companyReport = companyReport;
            GetSavePath();
        }
        private string ExportData(string path)
        {
            XlsFile xls = new XlsFile(System.IO.Path.Combine(Environment.CurrentDirectory, "result.xlsx"));
     
            for (int i = 0; i < companyReport.Count; i++)
            {
                xls.SetCellValue(i + 2, 1, companyReport[i].Name);
                xls.SetCellValue(i + 2, 2, companyReport[i].Address);
                xls.SetCellFromHtml(i + 2, 3, string.Format("<b>{0}</b>",companyReport[i].DepartmentName));
                xls.SetCellValue(i + 2, 4,companyReport[i].Post.ToString() );
                xls.SetCellValue(i + 2, 5, companyReport[i].CountUsers.ToString());
            }
            var fullpath = System.IO.Path.Combine(path, "test.xlsx");
            xls.Save(fullpath);

            return fullpath;
        }

        private void GetSavePath()
        {
            FolderBrowserDialog browse = new FolderBrowserDialog();
            DialogResult result = browse.ShowDialog();
            if (result == DialogResult.OK)
            {
                var folderName = browse.SelectedPath;

                var path = ExportData(folderName);

                if (MessageBox.Show("Открыть файл?", "Подтверждение!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (path != null)
                        Process.Start(path);
                    else
                        MessageBox.Show("Ошибка открытия файла!Файл не найден!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
