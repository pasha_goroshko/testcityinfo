﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCityInfo
{
    class RCompany
    {
        public string Name { get; set; }
        public int CountUsers { get; set; }
        public string Address { get; set; }
        public List<Rdepartment> rDepartment { get; set; }
    }
    class Rdepartment
    {
        /// <summary>
        /// Имя отдела.
        /// </summary>
        public string Name { get; set; }
        public int CountUsers { get; set; }
        public string Positions { get; set; }
    }
}
